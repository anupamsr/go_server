package main

import (
	"codeberg.org/anupamsr/go_server/server"
	"fmt"
	"github.com/pborman/getopt/v2"
	"log"
	"net/http"
	"os"
)

func main() {
	var port int
	set := getopt.New()
	set.FlagLong(&port, "port", 'p', "server port").Mandatory()
	set.Parse(os.Args)
	serverAddress := fmt.Sprintf("localhost:%d", port)
	mux := http.NewServeMux()
	serv := server.NewAlbumServer()
	mux.HandleFunc("/albums", serv.AlbumHandler)
	mux.HandleFunc("/albums/", serv.AlbumHandler)

	println("Starting server:", serverAddress)
	log.Fatal(http.ListenAndServe(serverAddress, mux))
}
