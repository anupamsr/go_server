package datalayer

import (
	"fmt"
	"sync"
)

// AlbumStore is a simple in-memory database of albums; AlbumStore methods are safe to call concurrently.
type AlbumStore struct {
	sync.Mutex

	albums map[int]Album
	nextId int
}

func New() *AlbumStore {
	albumStore := &AlbumStore{albums: make(map[int]Album), nextId: 0}
	// albums to seed record album store data.
	albumStore.CreateAlbum("Blue Train", "John Coltrane", 56.99)
	albumStore.CreateAlbum("Jeru", "Gerry Mulligan", 17.99)
	albumStore.CreateAlbum("Sarah Vaughan and Clifford Brown", "Sarah Vaughan", 39.99)
	return albumStore
}

// CreateAlbum creates a new album in the store.
func (as *AlbumStore) CreateAlbum(title string, artist string, price float64) int {
	as.Lock()
	defer as.Unlock()

	album := Album{
		Id:     as.nextId,
		Title:  title,
		Artist: artist,
		Price:  price}

	as.albums[as.nextId] = album
	as.nextId++
	return album.Id
}

// GetAlbum retrieves an album from the store, by id. If no such id exists, an error is returned.
func (as *AlbumStore) GetAlbum(id int) (Album, error) {
	as.Lock()
	defer as.Unlock()

	if t, ok := as.albums[id]; ok {
		return t, nil
	}
	return Album{}, fmt.Errorf("task with id=%d not found", id)

}

// DeleteAlbum deletes the album with the given id. If no such id exists, an error is returned.
func (as *AlbumStore) DeleteAlbum(id int) error {
	as.Lock()
	defer as.Unlock()

	if _, ok := as.albums[id]; !ok {
		return fmt.Errorf("task with id=%d not found", id)
	}

	delete(as.albums, id)
	return nil
}

// DeleteAllAlbums deletes all albums in the store.
func (as *AlbumStore) DeleteAllAlbums() error {
	as.Lock()
	defer as.Unlock()

	as.albums = make(map[int]Album)
	return nil
}

// GetAllAlbums returns all the albums in the store, in arbitrary order.
func (as *AlbumStore) GetAllAlbums() []Album {
	as.Lock()
	defer as.Unlock()

	allAlbums := make([]Album, 0, len(as.albums))
	for _, task := range as.albums {
		allAlbums = append(allAlbums, task)
	}
	return allAlbums
}

// GetAlbumsByTitle returns all the albums that have the given title, in arbitrary order.
func (as *AlbumStore) GetAlbumsByTitle(title string) []Album {
	as.Lock()
	defer as.Unlock()

	var albums []Album
	for _, task := range as.albums {
		if task.Title == title {
			albums = append(albums, task)
		}
	}
	return albums
}

// GetAlbumsByArtist returns all the albums that have the given artist, in arbitrary order.
func (as *AlbumStore) GetAlbumsByArtist(artist string) []Album {
	as.Lock()
	defer as.Unlock()

	var albums []Album
	for _, task := range as.albums {
		if task.Artist == artist {
			albums = append(albums, task)
		}
	}
	return albums
}

// GetAlbumsByPrice returns all the albums that have the given price, in arbitrary order.
func (as *AlbumStore) GetAlbumsByPrice(price float64) []Album {
	as.Lock()
	defer as.Unlock()

	var albums []Album
	for _, task := range as.albums {
		if task.Price == price {
			albums = append(albums, task)
		}
	}
	return albums
}
