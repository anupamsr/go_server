package server

import (
	"codeberg.org/anupamsr/go_server/datalayer"
	"encoding/json"
	"fmt"
	"log"
	"mime"
	"net/http"
)

type AlbumServer struct {
	store *datalayer.AlbumStore
}

func NewAlbumServer() *AlbumServer {
	return &AlbumServer{store: datalayer.New()}
}

func (as *AlbumServer) AlbumHandler(w http.ResponseWriter, req *http.Request) {
	if req.URL.Path == "/albums" || req.URL.Path == "/albums/" {
		if req.Method == http.MethodPost {
			as.createAlbumHandler(w, req)
		} else if req.Method == http.MethodGet {
			as.getAllAlbumsHandler(w, req)
		} else if req.Method == http.MethodDelete {
			as.deleteAllAlbumsHandler(req)
		} else {
			http.Error(w, fmt.Sprintf("expect method GET, DELETE or POST at /task/, got %v", req.Method), http.StatusMethodNotAllowed)
			return
		}
	}
}

func (as *AlbumServer) createAlbumHandler(respWriter http.ResponseWriter, req *http.Request) {
	log.Printf("handling task create at %s\n", req.URL.Path)

	// Types used internally in this handler to (de-)serialize the request and response from/to JSON.
	type RequestAlbum struct {
		Title  string  `json:"title"`
		Artist string  `json:"artist"`
		Price  float64 `json:"price"`
	}

	type ResponseId struct {
		Id int `json:"id"`
	}

	// Enforce a JSON Content-Type.
	contentType := req.Header.Get("Content-Type")
	mediaType, _, err := mime.ParseMediaType(contentType)
	if err != nil {
		http.Error(respWriter, err.Error(), http.StatusBadRequest)
		return
	}
	if mediaType != "application/json" {
		http.Error(respWriter, "expected application/json Content-Type", http.StatusUnsupportedMediaType)
		return
	}

	dec := json.NewDecoder(req.Body)
	dec.DisallowUnknownFields()
	var reqAlbum RequestAlbum
	if err := dec.Decode(&reqAlbum); err != nil {
		http.Error(respWriter, err.Error(), http.StatusBadRequest)
		return
	}

	id := as.store.CreateAlbum(reqAlbum.Title, reqAlbum.Artist, reqAlbum.Price)

	enc := json.NewEncoder(respWriter)
	if err = enc.Encode(ResponseId{Id: id}); err != nil {
		http.Error(respWriter, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (as *AlbumServer) getAllAlbumsHandler(respWriter http.ResponseWriter, req *http.Request) {
	log.Printf("handling get all tasks at %s\n", req.URL.Path)

	allAlbums := as.store.GetAllAlbums()

	enc := json.NewEncoder(respWriter)
	if err := enc.Encode(allAlbums); err != nil {
		http.Error(respWriter, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (as *AlbumServer) deleteAllAlbumsHandler(req *http.Request) {
	log.Printf("handling delete all tasks at %s\n", req.URL.Path)
	if err := as.store.DeleteAllAlbums(); err != nil {
		return
	}
}
